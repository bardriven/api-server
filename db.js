/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const mongoose = require("mongoose");
const config = require("./config");

let dbURI = config.db.mongodbURI;
mongoose.connect(dbURI, { useNewUrlParser: true });

mongoose.connection.on("connected", () => {
    console.log("Mongoose connected to " + dbURI);
});
mongoose.connection.on("error", (error) => {
    console.error("Mongoose connection error: " + error);
});
mongoose.connection.on("disconnected", () => {
    console.log("Mongoose disconnected");
});

process.on("SIGINT", () => {
    gracefulShutdown("app termination", () => { process.exit(0) });
});
function gracefulShutdown (msg, callback) {
    mongoose.connection.close(() => {
        console.log("Mongoose disconnected through " + msg);
        callback();
    });
}

require("./models/Thing");
require("./models/User");