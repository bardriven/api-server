/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const express = require("express");
const cookieParser = require("cookie-parser");
const logger = require("morgan");

let config = require("./config");
let authAPIKey = require("./middlewares/authAPIKey");
let thingsRouter = require("./routes/things");
let usersRouter = require("./routes/users");
let errorHandler = require("./helpers/errorHandler");

let app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.set("jwt-secret", config.token.secret);

app.use(authAPIKey);
app.use("/things", thingsRouter);
app.use("/users", usersRouter);
app.use(errorHandler);

module.exports = app;
