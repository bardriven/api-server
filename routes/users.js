/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const express = require("express");
const router = express.Router();
const { checkUserToken } = require("../middlewares/authToken");
const checkThing = require("../middlewares/checkThing");
const controller = require("../controllers/users");


// default require APIKey

// (GET) /users
router.get("/", controller.users);
// (GET) /users/:user_id
router.get("/:user_id", controller.user);
// (POST) /users
router.post("/", checkThing, controller.create);
// (PUT) /users/:user_id
router.put("/:user_id", controller.update);
// (PUT) /users/:email/things
router.put("/:user_id/things", checkThing, controller.addThing);
// (DELETE) /users/:email/things
router.delete("/:user_id/things/:thing_id", controller.removeThing);
// (DELETE) /users/:email
router.delete("/:user_id", controller.delete);

// (POST) /users/auth/token
router.post("/auth/token", controller.token);

// require userToken
router.use(checkUserToken);

// (GET) /users/auth/me
router.get("/auth/me", controller.me);
// (GET) /users/auth/refresh
router.get("/auth/refresh", controller.refresh);


module.exports = router;
