/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const express = require("express");
const router = express.Router();
const { checkThingToken } = require("../middlewares/authToken");
const controller = require("../controllers/things");


// default require APIKey

// (GET) /things
router.get("/", controller.things);
// (GET) /things/:thing_id
router.get("/:thing_id", controller.thing);
// (POST) /things
router.post("/", controller.create);
// (PUT) /things/:thing_id
router.put("/:thing_id", controller.update);
// (DELETE) /things/:thing_id
router.delete("/:thing_id", controller.delete);

// (POST) /things/auth/token
router.post("/auth/token", controller.token);

// require thingToken
router.use(checkThingToken);

// (GET) /things/auth/me
router.get("/auth/me", controller.me);
// (GET) /things/auth/refresh
router.get("/auth/refresh", controller.refresh);


module.exports = router;
