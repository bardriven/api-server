/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const jwt = require("jsonwebtoken");


module.exports.userToken = (user, secret) => {
    return new Promise((resolve, reject) => {
        jwt.sign(
            {
                _id: user.id,
                email: user.email,
                name: user.name,
                uType: user.uType,
                things: user.things
            },
            secret,
            {
                expiresIn: "7d",
                issuer: "mital",
                subject: "userInfo"
            }, (err, token) => {
                if (err) reject(err);
                resolve(token);
            });
    });
};

module.exports.thingToken = (thing, secret) => {
    return new Promise((resolve, reject) => {
        jwt.sign(
            {
                _id: thing.id,
                // key: thing.key,
                name: thing.name,
            },
            secret,
            {
                // expiresIn: "7d",
                issuer: "mital",
                subject: "thingInfo"
            }, (err, token) => {
                if (err) reject(err);
                resolve(token);
            });
    });
};

module.exports.createThingTokenSync = (thing, secret) => {
    return jwt.sign(
            {
                _id: thing.id,
                key: thing.key,
                name: thing.name,
            },
            secret,
            {
                expiresIn: "7d",
                issuer: "mital",
                subject: "thingInfo"
            });
};