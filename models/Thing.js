/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const bcrypt = require("bcrypt");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const { StatusError } = require("../helpers/CustomError");

let Thing = new Schema({
    name: { type: String, required: true },
    key: { type: String, required: true }
});

Thing.pre("save", function(next) {
    if (this.key) {
        bcrypt.hash(this.key, 10, (err, hash) => {
            this.key = hash;
            next();
        });
    }
});

Thing.statics.create = function (name, key) {
    let thing = new this({ name: name, key: key });
    return thing.save();
};

Thing.statics.findAllById = function (ids) {
    ids = ids.map(id => { return mongoose.Types.ObjectId(id) });
    return this.find({ _id: { "$in": ids } }).exec();
};

Thing.methods.verifyKey = function (key) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(key, this.key).then((res) => {
            if (res) resolve(this);
            else reject(new StatusError("키 인증 실패", 403));
        }).catch(reject);
    });
};

mongoose.model("Thing", Thing);