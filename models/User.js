/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const bcrypt = require("bcrypt");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Thing = mongoose.model("Thing");
const { StatusError } = require("../helpers/CustomError");


let User = new Schema({
    name: String,
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    uType: {
        type: String,
        "default": "user"
    },
    provider: { type: String, "default": "local" },
    things: [{
        type: Schema.Types.ObjectId,
        ref: "Thing",
        required: true
    }]
});

User.pre("save", function(next) {
    if (this.password) {
        bcrypt.hash(this.password, 10, (err, hash) => {
            this.password = hash;
            next();
        });
    } else next();
});

User.statics.create = function (email, password, provider, thing_id, uType = "user") {
    let user = new this({ email: email, password: password, provider: provider, uType: uType });
    if (uType !== "user") return user.save();
    else
        return new Promise((resolve, reject) => {
            Thing.findById(thing_id).exec((err, thing) => {
                if (err) reject(err);
                if (thing) {
                    user.things.push(mongoose.Types.ObjectId(thing_id));
                    user.save().then(resolve).catch(reject);
                }
                else reject(new StatusError("존재하지 않는 사물", 404));
            });
        });
};

User.methods.verifyPassword = function (password) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, this.password).then((res) => {
            if (res) resolve(this);
            else reject(new StatusError("인증 실패", 403));
        });
    });
};

User.methods.assignUType = function(uType) {
    this.uType = uType;
    return this.save();
};


mongoose.model("User", User);