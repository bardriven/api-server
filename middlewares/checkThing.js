/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const mongoose = require("mongoose");
const Thing = mongoose.model("Thing");
const { StatusError } = require("../helpers/CustomError");

module.exports = (req, res, next) => {
    let { thing_name, thing_key } = req.body;
    let name = thing_name;
    let key = thing_key;
    if (!name || !key) return next(new StatusError("입력 값 확인", 412));

    const check = thing => {
        if (!thing) return next(new StatusError("존재하지 않는 사물", 404));
        return thing.verifyKey(key);
    };

    const respond = (thing) => {
        req.thing = thing;
        next();
    };

    Thing.findOne({ name: name })
        .then(check)
        .then(respond)
        .catch(next);
};