/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const jwt = require("jsonwebtoken");
const { StatusError } = require("../helpers/CustomError");

module.exports.checkUserToken = (req, res, next) => {
    const usertoken = req.headers["x-access-usertoken"] || req.query.usertoken;

    if (!usertoken) next(new StatusError("권한이 없습니다."), 403);

    jwt.verify(usertoken, req.app.get("jwt-secret"), (err, decoded) => {
        if (err) next(err);
        req.decoded = decoded;
        next();
    });
};

module.exports.checkThingToken = (req, res, next) => {
    const thingtoken = req.headers["x-access-thingtoken"] || req.query.thingtoken;

    if (!thingtoken) next(new StatusError("권한이 없습니다."), 403);

    jwt.verify(thingtoken, req.app.get("jwt-secret"), (err, decoded) => {
        if (err) next(err);
        req.decoded = decoded;
        next();
    });
};