/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const mongoose = require("mongoose");
const Thing = mongoose.model("Thing");
const { StatusError } = require("../helpers/CustomError");
const { thingToken } = require("../helpers/createToken");
const respondJSON = require("../helpers/respondJSON");
// default require APIKey

// (GET) /things
// input: none
// output: things
module.exports.things = (req, res, next) => {
    const respond = things => {
        res.json(respondJSON(true, "things", null, things));
    };

    Thing.find({}, "-key")
        .then(respond)
        .catch(next);
};
// (GET) /things/:thing_id
// input: thing_id
// output: thing
module.exports.thing = (req, res, next) => {
    let id = req.params.thing_id;

    const respond = thing => {
        res.json(respondJSON(true, "thing", null, thing));
    };

    Thing.findById(id, "-key")
        .then(respond)
        .catch(next);
};

// (POST) /things
// input: name, key
// output: thing
module.exports.create = (req, res, next) => {
    let { name, key } = req.body;
    if (!name || !key) return next(new StatusError("입력 값 확인", 412));

    const create = thing => {
        if (thing) throw new StatusError("존재하는 사물", 409);
        return Thing.create(name, key);
    };

    const respond = thing => {
        thing.key = undefined;
        res.json(respondJSON(true, "생성 완료", null, thing));
    };

    Thing.findOne({ name: name }, "-key")
        .then(create)
        .then(respond)
        .catch(next);
};

// (PUT) /things/:thing_id // Not supported
module.exports.update = (req, res, next) => {
    return next(new StatusError("지원하지 않는 기능", 404));
};

// (DELETE) /things/:thing_id
// input: thing_id
// output: thing (removed)
module.exports.delete = (req, res, next) => {
     let id = req.params.thing_id;

    const remove = thing => {
        if (!thing) throw new StatusError("존재하지 않는 사물", 404);
        return thing.remove();
    };

    const respond = thing => {
        res.json(respondJSON(true, "삭제 완료", null, thing));
    };

    Thing.findById(id, "-key")
        .then(remove)
        .then(respond)
        .catch(next);
};

// (POST) /things/auth/token
// input: thing or name, key
// output: token
module.exports.token = (req, res, next) => {
    // let id = req.body.thing_id;
    // let name = req.body.name;
    // if (!id || !name) return next(new StatusError("입력 값 확인", 412));
    //
    // const check = thing => {
    //     if (!thing) throw new StatusError("존재하지 않는 사물", 404);
    //     if (thing.name === name) return Promise.resolve(thing);
    //     else throw new StatusError("올바르지 않은 사물 정보", 403);
    // };
    //
    // Thing.findById(id, "-key")
    //     .then(check)
    //     .then(token)
    //     .then(respond)
    //     .catch(next);
    let recvThing = req.body.thing;
    let recvName = req.body.name;
    let recvKey = req.body.key;


    const check = thing => {
        if (!thing) throw new StatusError("존재하지 않는 사물", 404);
        if (thing.name === recvThing.name) return Promise.resolve(thing);
        else throw new StatusError("올바르지 않은 사물 정보", 403);
    };

    const verifyKey = thing => {
        if (!thing) throw new StatusError("존재하지 않는 사물", 404);
        return thing.verifyKey(recvKey);
    };

    const token = thing => {
        return thingToken(thing, req.app.get("jwt-secret"));
    };

    const respond = token => {
        res.json(respondJSON(true, "인증 성공", null, token));
    };
    if (recvThing) {
        Thing.findById(recvThing._id, "-key")
            .then(check)
            .then(token)
            .then(respond)
            .catch(next);
    } else if (recvName && recvKey) {
        Thing.findOne({ name: recvName })
            .then(verifyKey)
            .then(token)
            .then(respond)
            .catch(next);
    }
};

// require thingToken

// (GET) /things/auth/me
// input: none
// output: thing
module.exports.me = (req, res, next) => {
    let id = req.decoded._id;
    if (!id) return next(new StatusError("올바른 토큰이 필요", 412));

    const respond = thing => {
        res.json(respondJSON(true, "성공", null, thing));
    };

    Thing.findById(id, "-key")
        .then(respond)
        .catch(next);
};

// (GET) /things/auth/refresh
// input: none
// output: token
module.exports.refresh = (req, res, next) => {
    let id = req.decoded._id;
    if (!id) return next(new StatusError("입력 값 확인", 412));

    const token = thing => {
        return thingToken(thing, req.app.get("jwt-secret"));
    };

    const respond = token => {
        res.json(respondJSON(true, "인증 성공", null, token));
    };

    Thing.findById(id, "-key")
        .then(token)
        .then(respond)
        .catch(next);
};