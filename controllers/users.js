/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const mongoose = require("mongoose");
const User = mongoose.model("User");
const { StatusError } = require("../helpers/CustomError");
const { userToken } = require("../helpers/createToken");
const respondJSON = require("../helpers/respondJSON");

// default require APIKey

// (GET) /users
// input: none
// output: users
module.exports.users = (req, res, next) => {
    const respond = users => {
        users.forEach(user => { return user.password = undefined });
        res.json(respondJSON(true, "users", null, users));
    };

    User.find({}, "-password")
        .then(respond)
        .catch(next);
};

// (GET) /users/:user_id
// input: user_id
// output: user
module.exports.user = (req, res, next) => {
    let user_id = req.params.user_id;
    if (!user_id) return next(new StatusError("입력 값 확인", 412));

    const populate = user => {
        return User.populate(user, { path: "things", select: "-key" });
    };

    const respond = user => {
        // user.password = undefined;
        // user.things.forEach(thing => { return thing.key = undefined });
        res.json(respondJSON(true, "user", null, user));
    };

    User.findById(user_id, "-password")
        .then(populate)
        .then(respond)
        .catch(next);
};

// (POST) /users
// input: email, password, password_check, (middleware) thing_name, thing_key
// output: user, thing
module.exports.create = (req, res, next) => {
    let thing = req.thing;
    let { email, password, password_check } = req.body;
    if (!email || !password || !password_check) return next(new StatusError("입력 값 확인", 412));
    if (password !== password_check) return next(new StatusError("비밀번호 확인", 412));

    const create = user => {
        if (user) throw new StatusError("존재하는 이메일", 409);
        return User.create(email, password, "local", thing.id);
    };

    const populate = user => {
        return User.populate(user, { path: "things", select: "-key" });
    };

    const respond = user => {
        user.password = undefined;
        res.json(respondJSON(true, "생성 완료", null, user));
    };

    User.findOne({ email: email }, "-password")
        .then(create)
        .then(populate)
        .then(respond)
        .catch(next);
};

// (PUT) /users/:user_id
// input: user_id, password, password_check, uType
// output: user
module.exports.update = (req, res, next) => {
    let user_id = req.params.user_id;
    let { password, password_check, uType } = req.body;
    if (password && password !== password_check) return next(new StatusError("입력 값 확인", 412));

    const change = user => {
        if (!user) throw new StatusError("존재하지 않는 유저", 404);
        if (password) user.password = password;
        if (uType) user.uType = uType;
        return user.save();
    };

    const respond = user => {
        res.json(respondJSON(true, "변경 완료", null, user));
    };

    User.findById(user_id, "-password")
        .then(change)
        .then(respond)
        .catch(next);
};

// (PUT) /users/:user_id/things
// input: user_id, (middleware) thing_name, thing_key
// output: user
module.exports.addThing = (req, res, next) => {
    let user_id = req.params.user_id;
    let thing = req.thing;
    let thing_id = req.thing._id;

    const change = user => {
        if (!user) throw new StatusError("존재하지 않는 유저", 404);
        let t = user.things.find(t => { return t.equals(thing_id); });
        if (t) throw new StatusError("존재하는 사물", 409);
        user.things.push(thing);
        return user.save();
    };

    const populate = user => {
        return User.populate(user, { path: "things", select: "-key" });
    };

    const respond = user => {
        res.json(respondJSON(true, "사물 추가 완료", null, user));
    };

    User.findById(user_id, "-password")
        .then(change)
        .then(populate)
        .then(respond)
        .catch(next);
};

// (DELETE) /users/:user_id/things/:thing_id
// input: user_id, thing_id
// output: user
module.exports.removeThing = (req, res, next) => {
    let { user_id, thing_id } = req.params;

    const change = user => {
        if (!user) throw new StatusError("존재하지 않는 유저", 404);
        let index = user.things.findIndex(tid => { return tid.equals(mongoose.Types.ObjectId(thing_id)); });
        if (index === -1) throw new StatusError("존재하지 않는 사물", 404);
        user.things.splice(index, 1);
        return user.save();
    };

    const populate = user => {
        return User.populate(user, { path: "things", select: "-key" });
    };

    const respond = user => {
        res.json(respondJSON(true, "사물 제거 완료", null, user));
    };

    User.findById(user_id, "-password")
        .then(change)
        .then(populate)
        .then(respond)
        .catch(next);
};

// (DELETE) /users/:user_id
// input: user_id
// output: user (removed)
module.exports.delete = (req, res, next) => {
    let user_id = req.params.user_id;

    const remove = user => {
        if (!user) throw new StatusError("존재하지 않는 유저", 404);
        return user.remove();
    };

    const respond = user => {
        res.json(respondJSON(true, "삭제 완료", null, user));
    };

    User.findById(user_id, "-password")
        .then(remove)
        .then(respond)
        .catch(next);
};

// (POST) /users/auth/token
// input: email, password
// output: token
module.exports.token = (req, res, next) => {
    let { email, password } = req.body;
    if (!email || !password) return next(new StatusError("입력 값 확인", 412));

    const check = user => {
        if (!user) throw new StatusError("존재하지 않는 이메일", 404);
        return user.verifyPassword(password);
    };

    const token = user => {
        return userToken(user, req.app.get("jwt-secret"));
    };

    const respond = token => {
        res.json(respondJSON(true, "인증 성공", null, token));
    };

    User.findOne({ email: email })
        .then(check)
        .then(token)
        .then(respond)
        .catch(next);
};

// require userToken

// (GET) /users/auth/me
// input: none
// output: user
module.exports.me = (req, res, next) => {
    let id = req.decoded._id;
    if (!id) return next(new StatusError("올바른 토큰이 필요", 412));

    const populate = user => {
        if (!user) return Promise.reject(new StatusError("존재하지 않는 유저", 404));
        return User.populate(user, { path: "things", select: "-key" });
    };

    const respond = user => {
        res.json(respondJSON(true, "성공", null, user));
    };

    User.findById(id, "-password")
        .then(populate)
        .then(respond)
        .catch(next);
};

// (GET) /users/auth/refresh
// input: none
// output: token
module.exports.refresh = (req, res, next) => {
    let id = req.decoded._id;
    if (!id) return next(new StatusError("입력 값 확인", 412));

    const token = user => {
        return userToken(user, req.app.get("jwt-secret"));
    };

    const respond = token => {
        res.json(respondJSON(true, "인증 성공", null, token));
    };

    User.findById(id, "-password")
        .then(token)
        .then(respond)
        .catch(next);
};